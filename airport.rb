## FCO.rb
require 'io/console'
require 'colorize'
require './utils.rb'
require './planegenerator.rb'
require 'net/http'
require 'json'
require 'uri'

## Variables initialization
### List of airport bound variables, runways and airlines
$runways = ["34L", "16L", "34R", "16L"]
$operators = ["AZ", "AA", "BA", "LH", "VY", "AF", "FR", "OS", "KL", "TK", "VY"]
$departure = ["FCO", "Leonardo Da Vinci International Airport", 0]
$destinations = [["MXP", "Milano Malpensa Airport", 511.75 ], ["CDG", "Paris Charles De Gaulle Airport", 1099.91], ["LHR", "London Heathrow Airport", 1444.96], ["JFK", "New York John Fitzgerald Kennedy International Airport", 6868.88] ]
### Flights array, actual structure: [flight1, flight2, flight3]
$activeflights=Array.new

def start
    Thread.new do
        system("ruby ./netcomm.rb")
    end
    sleep 10
    while true
        Thread.new do 
            planespawn()
        end
        sleep(rand 10..30)
    end
end

# Spawns a plane to launch
def planespawn()
    ## Generating flight number // move to utils
    flight = "#{$operators.sample}"+"#{randnum(4)}"
    ## Generating destination // Move to dedicated file, integrate with json listing
    arrivingloc = $destinations.sample
    ## Generating plane
    aircraft = planegenerator()
    ## Extracting data from variables
    speed = aircraft[2]
    destcode="#{arrivingloc[0]}"
    destname="#{arrivingloc[1]}"
    destdist= arrivingloc[2]
    ### DEBUG PRINT
    puts "Flight #{flight}, with destination #{arrivingloc[0]} - #{arrivingloc[1]} will be departing soon. The aircraft will be a #{aircraft[0]} \n \n".green
    ###############
    ## Sending data to API server
    sendplanedetails("#{flight}","#{destcode}","scheduled")
    ## Starting flight procedure
    sleep(10)
    flightstart(flight, destname, destdist, destcode, speed)
end


## Flight starting procedure. TODO: Show on billboard, calculate runway and taxi times
def flightstart(fl, destname, destdist, destcode, speed)
    sendplanedetails("#{fl}","#{destcode}","departing")
    runway=$runways.sample
    puts "Plane #{fl} Taxiing to runway #{runway}"
    sleep(10) 
    puts "Plane #{fl} Departing from runway #{runway}".red
    time= Time.now.getutc
    puts "Flight #{fl} started at #{time}"
    sendplanedetails("#{fl}","#{destcode}","departed")
    $activeflights.push("#{fl}")
    flightrunning(fl, destname, destdist, destcode, speed)
end

## Flight running time, need to calculate distance and set a travel time. TODO: rand for delays. # This feature is not essential, it will be needed later but will be counted in for performance.
def flightrunning(fl, destname, destdist, destcode, speed)
    if $activeflights.include? "#{fl}"
        puts "Flight is going to #{destname}"
        flighttime=(destdist/speed)*3600
        puts "Flight will last #{flighttime} seconds"
        sleep(flighttime)
        time= Time.now.getutc
        puts "Flight #{fl} has arrived at #{time}"
        $activeflights.delete("#{fl}")
    else
        puts "No active flight at the moment"
    end
end


# starts function
start