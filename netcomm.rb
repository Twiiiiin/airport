require 'sinatra'
require 'json'
require 'colorize'
require './utils.rb'

$planeslist={}

get '/' do
  "Hello, This is FCO Airport!"
end

get '/status' do
  planestatusprint = JSON.generate($planeslist)
  "#{planestatusprint}"
end

post '/plane' do
  newplane = JSON.parse(request.body.read)
  puts "I got some JSON: #{newplane.inspect}"
  plane= newplane['plane']
  destination= newplane['destination']
  status= newplane['status']
  ## Debug print - Uncomment to activate
  # puts "HI I AM THE SERVER, I GOT PLANE #{plane}, GOING TO #{destination}, WHICH HAS STATUS #{status}".green
  newplanestatus={ "#{plane}" => {'destination' => "#{destination}", 'status' => "#{status}"} }
  $planeslist.merge!(newplanestatus)
  'state 200 data received'
end 

