require 'net/http'
require 'json'

def randnum(input_n_digit)
    smallest_number_of_nplus1_digits = 10**input_n_digit
    smallest_number_of_n_digits = 10**(input_n_digit-1)
    random_n_digit_number = Random.rand(smallest_number_of_nplus1_digits-smallest_number_of_n_digits)+smallest_number_of_n_digits
    return random_n_digit_number
end

def sendplanedetails(code, destination, status)
    uri = URI('http://localhost:4567/plane')
    req = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
    req.body = {"plane": "#{code}", "destination": "#{destination}", "status": "#{status}"}.to_json
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
end
