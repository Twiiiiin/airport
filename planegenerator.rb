require 'json'
require 'colorize'
def planegenerator()
    ## generate planes with destinations
    planeslisthash= JSON.parse(File.read("./planeslist.json"))

    puts "Listing available planes:"
    puts "-------------------------"
    planeslisthash.each() do |plane|
        puts "#{plane}"
    end
    getrandomplane(planeslisthash)
end

def getrandomplane(hash)
    randplane=hash.keys[rand(hash.size)]
    puts "plane is #{randplane}"
    planedata= hash["#{randplane}"]
    ## Debug prints, uncomment below
    ################################
    # puts "Plane's data is #{planedata}"
    # name= planedata['name']
    # planeclass= planedata['planeclass']
    # speed= planedata['speed']
    # puts "DEBUG - Analyzed plane data is: NAME - #{name} CLASS - #{planeclass} SPEED - #{speed}".green
    # return planedata
end
#### Debug function
# main()